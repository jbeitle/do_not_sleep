
cc.Class({
    extends: cc.Component,

    properties: {
        teacher: null,
        stateMachine: null,
        targeted: false,
        isTouched: false,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.teacher = cc.find("Canvas/Level/Teacher").getComponent("teacher")
        this.stateMachine = this.node.getComponent("Student Sleep State Machine")
        this.node.on(cc.Node.EventType.TOUCH_END, this.touched, this)
        cc.director.getCollisionManager().enabled = true
    },

    // update (dt) {},

    
    touched () {
        this.isTouched = true
        this.teacher.throw(this.node.x, this.node.y)
    },
    
    onCollisionEnter () {
        if(this.isTouched == true){
            this.teacher.chalkHit(this.stateMachine.state)
            this.stateMachine.hitByChalk()
            this.isTouched = false
        }
    },

});