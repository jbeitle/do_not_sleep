
cc.Class({
    extends: cc.Component,

    properties: {
        anim: cc.Animation,
        state: cc.String,
    },

    start () {
        this.anim = this.getComponent(cc.Animation)
        this.idle()
    },

    // update (dt) {},
    idle () {
        this.state = "Idle"
        this.anim.play("Student Idle")
        this.scheduleOnce(this.fallingAsleep, Math.ceil(Math.random() * 17) + 3)
        //console.log(this.state)
    },

    fallingAsleep () {
        this.state = "Falling Asleep"
        this.anim.play("Student Falling Asleep")
        //console.log(this.state)
    },

    sleeping () {
        this.state = "Sleeping"
        this.anim.play("Student Sleeping")
        //console.log(this.state)
    },

    wakingUp () {
        this.state = "Waking Up"
        this.anim.play("Student Waking Up")
        //console.log(this.state)
    },

    hitByChalk () {
        if(this.state == "Falling Asleep" || this.state == "Sleeping"){
            this.wakingUp()
        }
    }

});
