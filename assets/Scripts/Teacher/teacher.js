
cc.Class({
    extends: cc.Component,

    properties: {
        ChalkPrefab: {
            default: null,
            type: cc.Prefab,
        },
        scoreBoard: cc.Label,
        livesLeft: cc.Label,
        loseInterface: cc.Node,
        chalk: cc.Node,
        score: cc.Integer,
        lives: cc.Integer,
    },

    start () {
        this.scoreBoard = cc.find("Canvas/Level/Score Board").getComponent(cc.Label)
        this.livesLeft = cc.find("Canvas/Level/Lives").getComponent(cc.Label)
        this.loseInterface = cc.find("Canvas/Level/Lose Interface")
        this.score = 0
        this.lives = 3
        this.loseInterface.active = false
        this.scoreBoard.string = this.score
        this.livesLeft.string = this.lives
    },

    throw (x, y) {
        if(this.chalk != null){
            return
        }
        var dx = this.node.x - x
        var dy = this.node.y - y
        var ang = (Math.atan(dx/dy) / Math.PI) * -180
        
        this.chalk = cc.instantiate(this.ChalkPrefab)
        this.chalk.parent = this.node
        this.chalk.angle = ang
    },

    chalkHit (state) {
        this.chalk.destroy()
        this.chalk = null
        if(state == "Falling Asleep" || state == "Sleeping"){
            this.score += 1
            this.scoreBoard.string = this.score
        }
        else{
            this.lives -= 1
            this.livesLeft.string = this.lives
            if(this.lives <= 0){
                this.loseInterface.active = true
            }
        }
    },

    reset () {
        cc.game.restart()
    }
});